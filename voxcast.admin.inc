<?php

/*
 *
 */
function voxcast_form_overview() {
  // Status if all account and devices settings are present.
  $status = variable_get('voxcast_key', '') != '' && variable_get('voxcast_secret', '') != '' && variable_get('voxcast_id', '') != '';
  
  if(!$status) {
    drupal_set_message(t('VoxCAST settings incomplete. Please edit them !here', array('!here' => l(t('here'), 'admin/settings/voxcast/settings'))), 'error');
    
    return array();
  }
  
  $device = voxcast_call_method('voxel.voxcast.ondemand.sites.list', array('device_id' => variable_get('voxcast_id', '')));
  
  foreach(array('@attributes', 'cache_settings', 'proxy_settings') as $key) {
    foreach($device['site'][$key] as $setting => $values) {
      $rows[] = array(ucfirst(str_replace('_', ' ', $setting)), implode('<br />', (array) $values));
    }
  }
  
  $form['overview'] = array(
    '#type' => 'fieldset',
    '#title' => $device['site']['@attributes']['name'],
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['overview']['data'] = array(
    '#value' => theme('table', array(t('Setting'), t('Value(s)')), $rows),
  );

  return $form;
}

/*
 *
 */
function voxcast_form_tools() {
  
  if(count($_SESSION['voxcast_txns'])) {
  
    $form['check_tx'] = array(
      '#type' => 'fieldset',
      '#title' => t('Follow up on a transaction.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    
    ksort($_SESSION['voxcast_txns']);
    $txns = array_reverse($_SESSION['voxcast_txns'], TRUE);
    
    foreach($txns as $time => $data) {
      $options[$data['id']] = $data['type'] .' at '. date('G:i:s', $time);
    }
    
    $form['check_tx']['tx_id'] = array(
      '#title' => t('Transaction'),
      '#type' => 'select',
      '#options' => $options,
      '#description' => t('Select the transaction of a previous request to check on it\'s status.'),
    );
  
    $form['check_tx']['check_tx'] = array(
      '#type' => 'submit',
      '#value' => t('Check transaction'),
    );

    $form['check_tx']['clear_tx'] = array(
      '#type' => 'submit',
      '#value' => t('Clear transactions list'),
    );
  }

/*  
  $form['test_url'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test URL for cacheability'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['test_url']['url'] = array(
    '#title' => t('Absolute URL'),
    '#type' => 'textfield',
    '#description' => t('Tests the cacheability of content on the origin.'),
  );

  $form['test_url']['verbosity'] = array(
    '#title' => t('Verbosity'),
    '#type' => 'select',
    '#default_value' => 'normal',
    '#options' => array('compact' => t('Compact'), 'normal' => t('Normal'), 'extended' => t('Extended')),
  );
*/
  $form['purge_paths'] = array(
    '#type' => 'fieldset',
    '#title' => t('Purge paths'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['purge_paths']['paths'] = array(
    '#title' => t('Paths'),
    '#type' => 'textarea',
    '#description' => t('A newline-separated list of paths to the content, e.g., "/path/to/content1\n/path/to/content2\n...", with at most 500 paths'),
  );

  $form['purge_paths']['purge_paths'] = array(
    '#type' => 'submit',
    '#value' => t('Purge paths'),
  );

  $form['purge_dir'] = array(
    '#type' => 'fieldset',
    '#title' => t('Purge directories'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['purge_dir']['dir_paths'] = array(
    '#title' => t('Directories'),
    '#type' => 'textarea',
    '#description' => t('A newline-separated list of paths to the content, e.g., "/path/to/content1\n/path/to/content2\n...", with at most 500 paths'),
  );

  $form['purge_dir']['purge_dir'] = array(
    '#type' => 'submit',
    '#value' => t('Purge directories'),
  );

  $form['purge_site'] = array(
    '#type' => 'fieldset',
    '#title' => t('Purge site'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['purge_site']['purge_site'] = array(
    '#type' => 'submit',
    '#value' => t('Purge site'),
  );

  return $form;

}

function voxcast_form_tools_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  switch($values['op']) {
    case t('Clear transactions list'):
      unset($_SESSION['voxcast_txns']);
      drupal_set_message(t('Transaction list cleared.'));
      break;
      
    case t('Check transaction'):
      voxcast_check_transaction($values['tx_id']);
      break;

    case t('Test URL'):
      voxcast_test_url($values['url'], $values['verbosity']);
      break;
      
    case t('Purge paths'):
      voxcast_purge_paths($values['paths']);
      break;

    case t('Purge directories'):
      voxcast_purge_directories($values['dir_paths']);
      break;

    case t('Purge site'):
      voxcast_purge_site();
      break;
  }
  
}

function voxcast_check_transaction($tx_id) {
  
  $vars = array(
    'transaction_id' => $tx_id,
  );
  
  $response = voxcast_call_method('voxel.voxcast.ondemand.content.transaction_status', $vars);
  
  if(is_array($response['err'])) {
    drupal_set_message($response['err']['@attributes']['msg'], 'error');
    return;
  }
  
  drupal_set_message(t('Type: !type<br />Status: !status', array('!type' => $response['transaction']['type'], '!status' => $response['transaction']['status'])));
}

function voxcast_test_url($url, $verbosity = 'normal') {
  
  $vars = array(
    'device_id' => variable_get('voxcast_id', ''),
    'origin_url' => $url,
    'verbosity' => $verbosity,
  );
  
  $response = voxcast_call_method('voxel.voxcast.ondemand.testing.cacheability', $vars);
  
  if(is_array($response['err'])) {
    drupal_set_message(t('VoxCAST error: !error', array('!error' => $response['err']['@attributes']['msg'])), 'error');
    return;
  }
  
  dump($response);
}

function voxcast_purge_paths($paths) {
  $paths = str_replace("\r", '', trim($paths));
  
  $vars = array(
    'device_id' => variable_get('voxcast_id', ''),
    'paths' => $paths,
  );
  
  $response = voxcast_call_method('voxel.voxcast.ondemand.content.purge_file', $vars);
  
  if(is_array($response['err'])) {
    drupal_set_message(t('VoxCAST error: !error', array('!error' => $response['err']['@attributes']['msg'])), 'error');
    return;
  }
  
  voxcast_record_transaction($response['transaction']['id'], 'File purge', $paths);
}

function voxcast_purge_directories($paths) {
  $paths = str_replace("\r", '', trim($paths));
  
  $vars = array(
    'device_id' => variable_get('voxcast_id', ''),
    'paths' => $paths,
  );
  
  $response = voxcast_call_method('voxel.voxcast.ondemand.content.purge_directory', $vars);
  
  if(is_array($response['err'])) {
    drupal_set_message(t('VoxCAST error: !error', array('!error' => $response['err']['@attributes']['msg'])), 'error');
    return;
  }
  
  voxcast_record_transaction($response['transaction']['id'], 'Directory purge', $paths);
}

function voxcast_purge_site() {
  $vars = array(
    'device_id' => variable_get('voxcast_id', ''),
  );
  
  $response = voxcast_call_method('voxel.voxcast.ondemand.content.purge_site', $vars);
  
  if(is_array($response['err'])) {
    drupal_set_message(t('VoxCAST error: !error', array('!error' => $response['err']['@attributes']['msg'])), 'error');
    return;
  }
  
  voxcast_record_transaction($response['transaction']['id'], 'Purge site');
}

function voxcast_record_transaction($id, $type, $data = FALSE) {
  $time = time();
  
  $type_string = t('!type', array('!type' => $type));
  
  // Collapse arrays into string.
  if($data) {
    $string = implode(', ', (array) $data);
    $data_string = substr(str_replace("\n", ', ', $string), 0, 40);

    // Assemble and truncate $data_string.
    $type_string .= ' '. t('(!string!ending)', array(
      '!string' => $data_string,
      '!ending' => (strlen($string) > 40) ? '...' : '',
    ));
  }
    
  $_SESSION['voxcast_txns'][$time] = array('id' => $id, 'type' => $type_string);
  
  drupal_set_message(t('Transaction successfully submitted! ID: !id', array('!id' => $id)));
}

/*
 *
 */
function voxcast_form_settings() {
  
  $key_and_pass = variable_get('voxcast_key', '') != '' && variable_get('voxcast_secret', '') != '';
  
  // If key and secret are present -- test, then get list of devices,
  // and then set if only once choice.
  if($key_and_pass) {

    // If key and secret are functional.
    if(voxcast_test_key_and_secret(variable_get('voxcast_key', ''), variable_get('voxcast_secret', ''))) {

      // Call method
      $devices = voxcast_call_method('voxel.devices.list');
      if($devices['@attributes']['stat'] == 'ok') {
        $options = array();
        
        // Only devices that are Cached Sites are added to $options
        foreach((array)$devices['devices'] as $device) {
          if($device['type'] == 'Cached Site') {
            $options[$device['@attributes']['id']] = $device['description'];
          }
        }
      }
      
      // If only one choice and choice has not been picked - set the choice.
      if(count($options) == 1 && variable_get('voxcast_id', '') == '') {
        drupal_set_message(t('VoxCAST ID automatically set to only choice: !id', array('!id' => current($options))));
        drupal_set_message( key($options));
        variable_set('voxcast_id', key($options));
      }
    }
  }
  
  // Status if all account and devices settings are present.
  $status = variable_get('voxcast_key', '') != '' && variable_get('voxcast_secret', '') != '' && variable_get('voxcast_id', '') != '';
  
  $form['cdn'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account information') . ($status ? ' '. t('(&#10004; complete)') : ''),
    '#collapsible' => TRUE,
    '#collapsed' => $status,
  );
  
  $form['cdn']['voxcast_key'] = array(
    '#title' => t('Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('voxcast_key', ''),
  );

  $form['cdn']['voxcast_secret'] = array(
    '#title' => t('Secret'),
    '#type' => 'password',
    '#description' => (variable_get('voxcast_secret', '') != '') ? t('Note: secret is stored but not shown') : '',
  );
  
  if(count($options)) {
    $form['cdn']['voxcast_id'] = array(
      '#title' => t('VoxCAST device'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => variable_get('voxcast_id', ''),
    );
  }
  
  $form['cdn']['keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get key and password'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('You can get the `key` and `secret` by entering your username, password, and selecting the `Get keys` button. For added security only the key and secret will be stored.'),
  );
  
  $form['cdn']['keys']['voxcast_username'] = array(
    '#title' => t('Username'),
    '#type' => 'textfield',
  );

  $form['cdn']['keys']['voxcast_password'] = array(
    '#title' => t('Password'),
    '#type' => 'password',
  );

  $form['cdn']['keys']['get_keys'] = array(
    '#value' => t('Get keys'),
    '#type' => 'submit',
  );

  $form['cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache control'),
    '#collapsible' => TRUE,
    '#collapsed' => !$status,
  );

  $form['cache']['voxcast_lifetime'] = array(
    '#title' => t('Cached items\' lifetime'),
    '#type' => 'textfield',
    '#default_value' => variable_get('voxcast_lifetime', 86400),
    '#description' => t('Enter the number of seconds a page should be cached before expiring.'),
  );

  $form['cache']['voxcast_clear_nodes_path'] = array(
    '#title' => t('Purge any node\'s path when updated or deleted.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('voxcast_clear_nodes_path', FALSE),
    '#description' => t('Check this box if you want, for every node type, to clear the cache for the node\'s URL on update.'),
  );

  $form['cache']['voxcast_verbose'] = array(
    '#title' => t('Show messages for each cache operation.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('voxcast_verbose', TRUE),
    '#description' => t('Check this box if you like seeing the hook_nodeapi activity via Drupal messages.'),
  );

  $form['cache']['voxcast_no_cache_paths'] = array(
    '#title' => t('Paths that should not be cached'),
    '#type' => 'textarea',
    '#default_value' => variable_get('voxcast_no_cache_paths', ''),
    '#description' => t('Enter one path per line. Paths should begin with a forward slash.  String matching is done from beginning of URL such that longer URLs will also match (ie. /my-favorite-ajax-url)'),
  );

  $form['cache']['voxcast_always_clear_on_save'] = array(
    '#title' => t('Paths that should purged on node save, update, or delete.'),
    '#type' => 'textarea',
    '#default_value' => variable_get('voxcast_always_clear_on_save', ''),
    '#description' => t('Enter one path per line. Paths should begin with a forward slash. If a path ends with `/*` then the directory and sub-directories will be cleared.<br />Do not use `/*` alone to purge the entire site.'),
  );

  $form['nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache control by node type'),
    '#collapsible' => TRUE,
    '#collapsed' => !$status,
  );

  $node_types = node_get_types('names');
  foreach($node_types as $type => $name) {
  
    $collapse = !variable_get('voxcast_clear_path_for_'. $type, FALSE) && variable_get('voxcast_always_clear_on_update_for_'. $type, '') == '';
    
    $form['nodes'][$type] = array(
      '#type' => 'fieldset',
      '#title' => $name,
      '#collapsible' => TRUE,
      '#collapsed' => $collapse,
    );
    
    $form['nodes'][$type]['voxcast_clear_path_for_'. $type] = array(
      '#title' => t('Purge node\'s path when updated.'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('voxcast_clear_path_for_'. $type, FALSE),
      '#description' => t('Check this box if you want to clear the CDN cache of this node type\'s URL on update. This will override the general option.'),
    );

    $form['nodes'][$type]['voxcast_always_clear_on_update_for_'. $type] = array(
      '#title' => t('Paths that should purged on node update or save.'),
      '#type' => 'textarea',
      '#default_value' => variable_get('voxcast_always_clear_on_update_for_'. $type, ''),
      '#description' => t('Enter one path per line. Paths should begin with a forward slash. If a path ends with `/*` then the directory and sub-directories will be cleared.'),
    );
  
  }
  
  return system_settings_form($form);
}

function voxcast_form_settings_validate($form, &$form_state) {
  
  // Fill in hidden password.
  if($form_state['values']['voxcast_secret'] == '') {
    $form_state['values']['voxcast_secret'] = variable_get('voxcast_secret', '');
  }
  
  $values = $form_state['values'];
  
  if($values['op'] == $values['get_keys']) {
    $keys = voxcast_get_key_and_secret($values['voxcast_username'], $values['voxcast_password']);
    
    if($keys['key'] == '' || $keys['secret'] == '') {
      form_set_error('voxcast_username', t('Cannot connect to VoxCAST API. Check your username/password or if too many attempts have been made - wait.'));
      form_set_error('voxcast_password', ' ');
      array_pop($_SESSION['messages']['error']);
      return;
    }
    else {
      drupal_set_message(t('Successfully retrieved keys.'));
      $form_state['values']['voxcast_key'] = $keys['key'];
      $form_state['values']['voxcast_secret'] = $keys['secret'];
    }
  }
  elseif($values['op'] != $values['reset']) {
    // Are both values set? If so, test.
    if(($values['voxcast_key'] != '') || ($values['voxcast_secret'] != '')) {
      if(!voxcast_test_key_and_secret($values['voxcast_key'], $values['voxcast_secret'])) {
        form_set_error('voxcast_key', t('Key and or Secret incorrect.'));
        form_set_error('voxcast_secret', ' ');
        array_pop($_SESSION['messages']['error']);
      }
      else {
        // drupal_set_message('hAPI access confirmed.');
      }
    }
  }
  
  if(!is_numeric($values['voxcast_lifetime'])) {
    form_set_error('voxcast_lifetime', t('Please enter a number.'));
  }
  
  // Do not save username or password.
  unset($form_state['values']['voxcast_username']);
  unset($form_state['values']['voxcast_password']);
}

